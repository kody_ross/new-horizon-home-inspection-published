<?php

class Page {
    function __construct($path, $title, $body)
    {
        $this->path = $path;
        $this->title = $title;
        $this->body = $body;
    }

    function getPath() {
        return $this->path;
    }

    function getTitle() {
        return $this->title;
    }

    function getBody() {
        return $this->body;
    }
}

class Field {
    function __construct($name, $value)
    {
        $this->name = $name;
        $this->value = $value;
    }

    function getName() {
        return $this->name;
    }

    function getValue() {
        return $this->value;
    }
}

class Site {
    function __construct($pages, $fields)
    {
        $this->pages = array();
        $this->fields = array();

        // Index pages by path
        foreach($pages as $page) {
            $this->pages[$page->path] = $page;
        }

        // Index fields by name
        foreach($fields as $field) {
            $this->fields[$field->name] = $field;
        }
    }

    function getPages() {
        return $this->pages;
    }

    function getPage($path) {
        // TODO: what does this return if the key is not found?
        return $this->pages[$path];
    }

    function getFields() {
        return $this->fields;
    }

    function getField($name) {
        // TODO: what does this return if the key is not found?
        return $this->fields[$name];
    }
}

// TODO: in future some data will be pulled from database connection (probably PDO based)
$s = new Site(
    array(
        new Page('/', 'Home', "This is the body of the home page."),
        new Page('/about-us', 'About Us', "This is an about us page."),
        new Page('/services', 'Services', "This is the body of the services page.")
    ),
    array(
        new Field("phone", "519-338-9998"),
        new Field("email", "sales@mintoautocentre.ca"),
        new Field("facebook", "Mintoautocentre"),
        new Field("brand", "Minto Auto Centre"),
    )
);

// TODO: check some things that could affect headers here, so we can deal with them before response body is output
// TODO: Could have test script running on live set to make sure 404s are being returned properly

?>

<html>
<head>
    <title>McGarry Mediations</title>
    <link rel="stylesheet" href="/style.css">
    <script src="/static/js/jquery-1.12.3.min.js"></script>
    <script src="/static/js/bootstrap.min.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Crete+Round" rel="stylesheet">

    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body style="padding-top: 30px;">

<div class="container">
    <div class="row" style="padding-bottom: 15px;">
        <div class="col-md-2">
            <img class="img-responsive" src="/static/logo.png">
        </div>
        <div class="col-md-10">
            <ul class="nav nav-pills pull-left">
                

               
                </li>
            </ul>
            <div class="pull-right text-primary">
            <h3><a href="tel:5193343660">(519) 434-1221</a></h3>
            <div>200 Queens Ave., Suite 304, London, ON N6A 1J3</div>
            <a href="mailto:janice@mcgarrymediations.com">janice@mcgarrymediations.com</a>
        </div>
    </div>
</div>


<?php if($_SERVER["REQUEST_URI"] === "/"): ?>

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <h2>Experienced Mediator and Arbitrator</h2>
            <p style="font-size: 2.5rem">Throughout his remarkable career, the Hon. John F. McGarry Q.C. has earned a
            reputation as a fair and thorough mediator determined to resolve even the most
            complex of cases. His success has been shaped in his four decades of experience as
            both a practicing lawyer, drug Prosecutor and a Superior Court Judge. John's skilled
            approach creates an environment of respect and patience that is focused on achieving
            results.</p>
        </div>

        <div class="col-md-6">            
            <img src="/static/mcgarry.png" style="width: 90%; margin-top: 0px; margin-bottom: 10px;">
            <h3>Hon John F McGarry , Q.C.</h3>
            <h5>Retired Superior Court Judge</h5>
        </div>
    </div>

    <BR>
    <BR>

    <div class="row">
        <div class="col-md-6">
            <a name="reviews"></a>
          
            <strong><p>Four decades of experience - both as a practicing lawyer and a Superior Court Judge - have
            shaped John's approach to mediation and arbitration.</p></strong>
            <BR>
            <p>Prior to his appointment to the judiciary, he practiced in many aspects of the law including
            corporate commercial finance, international acquisitions, finance, bankruptcy, construction and
            labour law. In addition, early in his career, he was both a Federal Drug Prosecutor and Assistant
            Crown Attorney.</p>
            <BR>
            <p>John's varied background was a significant benefit to him on the bench and it has been an asset
            in his work as a mediator and arbitrator as well.</p>
            <br>
            <p>As a judge, John focused on pre-trials to settle cases. When appointed a supernumerary judge,
            he developed a judicial mediation methodology that resolved the most complex cases. This
            initiative was outlined in a Canadian Lawyer article titled On the Right Track.</p>
            <BR>
            <p>Now in retirement, John has mediated a full range of cases including personal injury cases, slip
            and fall, motor vehicle negligence, wrongful dismissal, medical malpractice, commercial disputes
            and sexual assault claims.</p>
            <BR>
            <p>John's success as a mediator and arbitrator is credited to his expertise in creating an
            environment that is focused on achieving results.</p>
            <BR>

        </div>
        <div class="col-md-6">
            <div class="green">
            <h4>Resolutions for a Full Range of Cases</h4>
                <ul>
                    <li>Professional negligence</li>
                    <li>Estates</li>
                    <li>Employment law</li>
                    <li>Insurance litigation including personal injuries</li>
                    <li>Corporate and commercial law</li>
                    <li>Sexual assault claims</li>
                </ul>
            </div>
            <a name="book-now"></a>
            <h2>Contact Us</h2>
            <p>Enter your information and we will contact you to book an inspection.</p>
            <form method ="post" action="/emailform.php">
                <div class="form-group">
                    <!-- <label for="InputName">Name</label> -->
                    <input type="text" class="form-control" id="InputName" name="InputName" placeholder="Name">
                </div>
                <!-- <div class="form-group">
                    <label for="InputTel">Phone Number</label>
                    <input type="tel" class="form-control" id="InputTel" name="InputTel" placeholder="Phone Number">
                </div> -->
                <div class="form-group">
                    
                    <input type="email" class="form-control" id="InputEmail" name="InputEmail" placeholder="Email Address">
                </div>
                 
                <div class="form-group">
                   <!--  <label for="InputMessage">Message</label> -->
                    <textarea class="form-control" id="InputMessage"  name="InputMessage" maxlength ="250" ></textarea>
                </div>
                <button type="submit" name= "formSubmit" class="btn btn-primary">Send</button>
            </form>
        </div>
        <!-- <div class="col-md-6">
            <h3>Contact Us</h3> -->
            <!-- <address>
                200 Queens Ave. Suite 304, London, ON N6A 1J3<br>
                
                <abbr title="Phone">P:</abbr> <a href="tel:5194341221">(519) 434-1221</a>
            </address>

            <address>
                <strong>Jim Rabe</strong><br>
                <a href="mailto:janice@mcgarrymediations.com">janice@mcgarrymediations.com</a><BR>
<!--                 <abbr title="Cell">C:</abbr> <a href="tel:5193755358">(519) 375-5358
            </address> -->
       <!--  </div> -->
    </div>

    <BR>

    

    
    </div>

</div>

<?php endif ?>

<div class="container-fluid" style="padding: 40px; margin-top: 50px; background: #154387;">
        <img src="/static/mcgarry_wm.png" style="float:left;width: 25%; margin-top: 0px; margin-bottom: 10px;">
        <address class="text-center" style="text-align:left;">
        <strong>200 Queens Ave. Suite 304, London, ON N6A 1J3</strong>
        <BR>        
        <a href="tel:5194341221">t. 519-434-1221</a>
        <BR>
        <a href="mailto:janice@mcgarrymediations.com">janice@mcgarrymediations.com</a>
        </address>
</div>

</body>
</html>
