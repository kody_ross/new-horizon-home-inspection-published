<?php
error_reporting(E_ALL);


$errorMessage = "";

if (empty($_POST['InputName'])) {
    $errorMessage .= "<li>Name field is empty.</li>";
}
if (!filter_var($_POST['InputEmail'], FILTER_VALIDATE_EMAIL)) {
    $errorMessage .= "<li>Email is invalid.</li>";
}
if (empty($_POST['InputMessage'])) {
    $errorMessage .= "<li>Please leave us a message.</li>";
}
if ($errorMessage) {
    echo("<p>There was an error with your form:</p>\n");
    echo("<ul>" . $errorMessage . "</ul>\n");
}
    // print_r($_POST);
    // var_dump(isset($_POST['formSubmit']));

if (!$errorMessage) {
    $to = "kody@osiminteractive.com";
    $varName = $_POST['InputName'];
    $varEmail = $_POST['InputEmail'];
    $varMessage = $_POST['InputMessage'];
    $message = $varName . " wrote the following: \n" . $varMessage  ."\n". "Email: " . $varEmail ." \n" ;

    $headers = "From:" . $varEmail;
    mail($to, 'contact - ' . $varName, $message);
    echo "Your information has been sent. Thank you.";
}
